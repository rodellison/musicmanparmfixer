//Copyright 2013-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//Licensed under the Apache License, Version 2.0 (the "License"). 
//You may not use this file except in compliance with the License. 
//A copy of the License is located at
//
//    http://aws.amazon.com/apache2.0/
//
//or in the "license" file accompanying this file. This file is distributed 
//on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
//either express or implied. See the License for the specific language 
//governing permissions and limitations under the License.

//Get modules.
var express = require('express');
var app = express();

var routes = require('./routes');
var path = require('path');

var favicon = require('serve-favicon');
app.use(favicon(path.join(__dirname,'static','favicon.ico')));
app.use(express.static(path.join(__dirname, `/static`)));

var fs = require('fs');
//Read config values from a JSON file.
var config = fs.readFileSync('./app_config.json', 'utf8');
console.log('Reading app_config.json: ' + config.toString());
config = JSON.parse(config);

var AWS = require('aws-sdk');
//var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

app.set('views', `${__dirname}/views`);
app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride('X-HTTP-Method-Override'));

//Create DynamoDB client and pass in region.
var db = new AWS.DynamoDB({region: config.AWS_REGION});
//Create SNS client and pass in region.
var sns = new AWS.SNS({ region: config.AWS_REGION});

//GET home page.
app.get('/', routes.index);

//POST signup form.
//Note when going through AWS API Gateway/Lambda - on the client side post, need to include /latest/signup in the 
//index.html file - e.g.         $.post( "/latest/signup", $("#signupForm").serialize(),
//but inside the code here, remove the /latest and just use the /signup part.   Something to do with how API Gateway is
//channeling versions in the URL.
app.post('/signup', function(req, res) {

  var InvalidDataField = req.body.InvalidData,
      ValidDataField = req.body.ValidData;
  console.log('Calling signup function..');
  signup(InvalidDataField.toLowerCase(), ValidDataField, res);
  
});

//Add signup form data to database.
var signup = function (invalidDataSubmitted, validDataSubmitted, res) {
  var formData = {
    TableName: config.MUSICMAN_CORRECTION_TABLE,
    Item: {
      SongKickInvalidParm: {'S': invalidDataSubmitted},
      SongKickValidParm: {'S': validDataSubmitted}
    },
    "ConditionExpression": "SongKickInvalidParm <> :f",
    "ExpressionAttributeValues": {
      ":f": {"S": invalidDataSubmitted}
    }
  };

  db.putItem(formData, function(err, data) {
    if (err) {
      if (err.code == "ConditionalCheckFailedException") {
        res.sendStatus(400);
        console.log('Error adding item to database: A duplicate value already exists. Error: ', err);
      } else {
        res.sendStatus(400);
        console.log('Error adding item to database: ', err);
      }
    } else {
      res.sendStatus(200);
      console.log('Form data added to database.');
      var snsMessage = 'New MusicManParmFix value added - ' + invalidDataSubmitted;
      sns.publish({ TopicArn: config.NEW_MUSICMAN_CORRECTION_TOPIC, Message: snsMessage }, function(err, data) {
        if (err) {
          console.log('Error publishing SNS message: ' + err);
        } else {
          console.log('SNS message sent successfully.');
        }
      });
    }
  });
};

//If running outside of the AWS Lambda + API Gateway configuration, need to uncomment the lines and create a server.
//var http = require('http');
//app.set('port', process.env.PORT || 3000);
//http.createServer(app).listen(app.get('port'), function(){
//  console.log('Express server listening on port ' + app.get('port'));
//});

//If running in AWS Lambda + API Gateway - and using Claudia.js - uncomment this line, but comment the server lines
//immediately above
module.exports = app;
