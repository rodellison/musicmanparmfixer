MusicManParmFixer
=================

This Node.js application uses the [Express](http://expressjs.com/) framework and [Bootstrap](http://getbootstrap.com/) to provide a simple interface that can allow people who use the **Alexa Skill MusicMan** - a means to provide parameter correction values when Artist names or Venue names aren't detected by Alexa correctly. 
e.g. Rianna should be Rihanna.
The app is deployed to AWS Lambda with a front end through AWS API Gateway [leveraging techniques explained in this article](https://aws.amazon.com/blogs/compute/going-serverless-migrating-an-express-application-to-amazon-api-gateway-and-aws-lambda/) and using [CLaudia.js](https://claudiajs.com).
The functionality of the app is to store user provided fixes/input  in an [Amazon DynamoDB](http://aws.amazon.com/dynamodb/) database. 
Finally, when corrections are submitted (successful or not), the [Amazon Simple Notification Service (SNS)](http://aws.amazon.com/sns/)  fires a note off to the developer who can then further test the suggestions.

## app_config.json
The application stores connection info in a JSON file, app_config.json. The key-value pairs are as follows:
* AWS_REGION: the AWS region code, e.g. 'us-west-2'
* MUSICMAN_CORRECTION_TABLE: the name of the DynamoDB table where the app stores the MusicManParmFixer data (e.g. 'my-ddb-table')
* NEW_MUSICMAN_CORRECTION_TOPIC: The [ARN](http://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html) of the SNS topic that the app uses to send notifications (e.g. 'arn:aws:sns:us-west-2:123456789012:my-supercool-app')



